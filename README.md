uk.org.bobulous.java.startup
============================

<p>This repository has moved to <a href="https://codeberg.org/Bobulous/MainArgsHandler">Codeberg</a>. This GitLab copy has been archived (read-only mode). To contribute to this project please visit the <a href="https://codeberg.org/Bobulous/MainArgsHandler">Codeberg repository</a>.</p>
